import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PruebaaComponent } from './pruebaa/pruebaa.component';
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'configuracion',
        component: PruebaaComponent
      },
      {
        path: 'ds',
        component: PruebaaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PruebaRoutingModule { }
