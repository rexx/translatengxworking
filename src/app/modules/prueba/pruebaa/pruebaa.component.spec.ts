import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaaComponent } from './pruebaa.component';

describe('PruebaaComponent', () => {
  let component: PruebaaComponent;
  let fixture: ComponentFixture<PruebaaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PruebaaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
