import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-pruebaa',
  templateUrl: './pruebaa.component.html',
  styleUrls: ['./pruebaa.component.css']
})
export class PruebaaComponent implements OnInit {

  constructor(public translate: TranslateService) { }

  ngOnInit() {
  }

}
