import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'testtraductor';
  constructor(private router: Router,public translate: TranslateService){
    translate.addLangs(['en', 'es']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    const setLang = browserLang.match(/en|es/) ? browserLang : 'en';
    translate.use(setLang);
    localStorage.setItem('lang',setLang);
  }
}
