import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PruebaaComponent } from './pruebaa/pruebaa.component';

import { RouterModule } from '@angular/router';
import { PruebaRoutingModule } from './prueba-routing.module';
import { SharedModule } from '../shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PruebaRoutingModule,
    SharedModule
  ],
  declarations: [PruebaaComponent]
})
export class PruebaModule { }
